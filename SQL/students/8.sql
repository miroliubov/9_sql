SELECT student.name 

FROM students.student 

INNER JOIN students.marks 
ON marks.student_id = student.student_id 

WHERE mark = 2 
GROUP BY student.name 

HAVING COUNT(mark) = 2 

ORDER BY student.name;