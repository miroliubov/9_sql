SELECT student.name, marks.mark 
FROM students.student, students.marks 
WHERE marks.subject = 'Java' 
AND marks.student_id = student.student_id;