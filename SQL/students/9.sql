SELECT marks.subject 

FROM students.marks
 
WHERE mark = 2 

GROUP BY marks.subject 

HAVING COUNT(mark) > 2 

ORDER BY marks.subject;