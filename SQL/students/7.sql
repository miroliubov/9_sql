SELECT student.name, COUNT(marks.mark) 

FROM students.student, students.marks 

WHERE marks.student_id = student.student_id 
GROUP BY student.name;