SELECT student.group, marks.subject, AVG(marks.mark) 

FROM students.student, students.marks 

WHERE marks.subject = 'Java' 
AND marks.student_id = student.student_id 
GROUP BY student.group;