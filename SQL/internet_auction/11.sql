DELETE bid
FROM internet_auction.bids AS bid 
INNER JOIN internet_auction.items AS item 
ON bid.items_item_id = item.item_id
WHERE bid.users_user_id = 11;

DELETE FROM items
WHERE users_user_id = 11