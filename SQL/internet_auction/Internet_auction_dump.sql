-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: internet_auction
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL,
  `bid_date` date DEFAULT NULL,
  `bid_value` double DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  `items_item_id` int(11) NOT NULL,
  PRIMARY KEY (`bid_id`),
  KEY `fk_bids_users_idx` (`users_user_id`),
  KEY `fk_bids_items1_idx` (`items_item_id`),
  CONSTRAINT `fk_bids_items1` FOREIGN KEY (`items_item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `fk_bids_users` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` VALUES (1,'2018-05-15',50000,2,1),(2,'2018-06-10',100000,4,1),(3,'2018-06-13',50000,7,1),(4,'2018-03-10',20000,5,2),(5,'2018-03-15',40000,2,2),(6,'2018-08-11',10000,3,10),(7,'2018-08-19',10000,9,10),(8,'2018-10-04',1000,2,3),(9,'2018-10-06',3000,7,3),(10,'2018-10-16',4000,9,3),(11,'2018-10-18',1000,4,3),(12,'2018-05-17',15000,2,5),(13,'2018-05-19',30000,6,5),(14,'2018-02-12',55000,8,4),(15,'2018-04-15',25000,5,8);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_price` double DEFAULT NULL,
  `bid_increment` double DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `stop_date` date DEFAULT NULL,
  `by_it_now` binary(1) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_items_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_items_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Mazda CX-6','Красный, без повреждений, 2017',600000,50000,'2018-05-14','2018-06-14',NULL,1),(2,'Audi Q5','Белый, без повреждений, 2013',400000,20000,'2018-03-07','2018-03-27',NULL,1),(3,'Lada Granta','Серебристый, битый, 2015',50000,1000,'2018-10-02','2019-01-20',NULL,3),(4,'Honda CR-V','Серый, без повреждений, 2018',700000,55000,'2018-01-12','2018-02-23',NULL,3),(5,'KIA Sportage','Синий, без повреждений, 2012',300000,15000,'2018-05-16','2018-05-20',NULL,5),(6,'Subaru Forester','Черный, битый, 2008',15000,500,'2018-06-02','2018-06-15',NULL,5),(7,'Skoda Rapid','Красный, без повреждений, 2014',350000,15000,'2018-07-25','2018-08-17',NULL,6),(8,'Volkswagen Polo','Белый, без повреждений, 2016',630000,25000,'2018-04-01','2018-04-16',NULL,6),(9,'KIA Rio','Серебристый, битый, 2018',250000,15000,'2018-11-04','2018-12-11',NULL,8),(10,'Toyota Corolla','Черный, без повреждений, 2010',210000,10000,'2018-08-09','2018-08-29',NULL,10);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `billing_address` varchar(100) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Вячеслав Миролюбов','ул.Новостроительная, 15-15, Россия, Удмуртская республика, Ижевск','slava1337','qwerty'),(2,'Антон Чехов','ул.Мужвайская, 3, Россия, Удмуртская республика, Ижевск','antosha228','qwerty1'),(3,'Александр Пушкин','ул. Пушкинская, 19-19, Россия, Московская область, Москва','SaNyOk','qwerty123'),(4,'Сергей Мезенцев','ул. Пушкина, 22-11, Россия, Московская область, Москва','SeryogaMoskwa','qwerty12345'),(5,'Андрей Андреев','ул.Тверская, 15-1, Россия, Московская область, Москва','andrey01','drtjhse43543'),(6,'Нариман Намазов','ул.Советская, 13-13, Россия, Кировская область, Киров','NarimanAbu2','l537tj547'),(7,'Игорь Енакентьев','ул.Максима Горького, 125-128, Россия, Свердловская область, Екатеринбург','igoryan12','qjd53hgs'),(8,'Евгений Кузнецов','ул.Красноармейская, 213-17, Россия, Московская область, Москва','evgeniy16','sn5ygsb'),(9,'Олег Коротков','ул.Красногеройская, 82-9, Россия, Ленинградская область, Выборг','olejjka','nan5efgafg'),(10,'Георгий Лосев','ул.Герцена, 212-11, Россия, Ульяновская область, Ульяновск','gooshaa','a34tyareg'),(11,'Александр Друзь','ул. Сергея Дружко, 12-228, Россия, Тверская область, Тверь','gruzzd','jnumkne45gv3');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-09 19:05:16
