SELECT title, MAX(bid_value) 
FROM internet_auction.bids, internet_auction.items
WHERE items.item_id = bids.items_item_id
GROUP BY title;